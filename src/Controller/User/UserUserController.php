<?php

namespace App\Controller\User;

use App\Controller\ApiController;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserUserController extends ApiController
{
    /**
     * Get all users from db
     *
     * @Route("/", name="user_index", methods={"GET"})
     *
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function index(UserRepository $userRepository)
    {
        $users = $userRepository->transformAll();

        return $this->respond($users);
    }

    /**
     * Get specific user from db
     *
     * @Route("/{id}", name="user_show", methods={"GET"})
     *
     * @param User $user
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function show(User $user, UserRepository $userRepository)
    {
        return $this->respond($userRepository->transform($user));
    }

    /**
     * Create new user
     *
     * @Route("/new", name="user_new", methods={"POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        //todo add email and password validation

        if (!$request->get('email')) {
            return $this->respondValidationError('Email is empty!');
        }

        if (!$request->get('password')) {
            return $this->respondValidationError('Password is empty!');
        }

        $user = new User();
        $user->setName($request->get('name'));
        $user->setEmail($request->get('email'));
        $user->setPassword($encoder->encodePassword($user, $request->get('password')));
        $user->setCreatedAt(new DateTime());
        $user->setUpdatedAt(new DateTime());
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->respondCreated($userRepository->transform($user));
    }

    /**
     * Edit user data
     *
     * @Route("/{id}/edit", name="user_edit", methods={"PUT"})
     *
     * @param Request $request
     * @param User $user
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws \Exception
     */
    public function edit(Request $request, User $user, UserRepository $userRepository, UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
    {
        //todo: 1)Add email and password validation 2)Remove duplicates - $user->setUpdatedAt(new DateTime())

        if ($request->get('name')) {
            $user->setName($request->get('name'));
            $user->setUpdatedAt(new DateTime());
        }

        if ($request->get('email')) {
            $user->setEmail($request->get('email'));
            $user->setUpdatedAt(new DateTime());
        }

        if ($request->get('password')) {
            $user->setPassword($encoder->encodePassword($user, $request->get('password')));
            $user->setUpdatedAt(new DateTime());
        }
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->respond($userRepository->transform($user));
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param User $user
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function delete(User $user, EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->respond($userRepository->transform($user));
    }
}
